# ec2/main.tf

data "aws_ami" "mosar_server_ami" {
  owners      = var.ami_owners
  most_recent = true

  filter {
    name   = "name"
    values = [var.ami_name_string]
  }
}

resource "random_id" "server_id" {
  for_each    = var.nodes
  byte_length = 4
  keepers = {
    key_name = each.value.key_name
    ami_id   = data.aws_ami.mosar_server_ami.id
  }
}

resource "aws_instance" "node" {
  for_each                    = var.nodes
  ami                         = random_id.server_id[each.key].keepers.ami_id
  instance_type               = each.value.instance_type
  key_name                    = random_id.server_id[each.key].keepers.key_name
  subnet_id                   = each.value.subnet_ids[0]
  private_ip                  = each.value.private_ip
  vpc_security_group_ids      = each.value.vpc_security_group_ids
  associate_public_ip_address = each.value.associate_public_ip_address
  iam_instance_profile        = each.value.iam_instance_profile
  source_dest_check           = each.value.source_dest_check
  root_block_device {
    volume_size = each.value.root_block_device_vol_size
  }

  tags = {
    Name        = "${each.key}${random_id.server_id[each.key].dec}"
    Environment = var.environment
    ManagedBy   = var.managed_by
  }
  user_data = templatefile(each.value.user_data_file_path, each.value.script_vars)
}
