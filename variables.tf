# ec2/variables.tf

variable "ami_name_string" {}
variable "ami_owners" {}
variable "nodes" {}
variable "environment" {}
variable "managed_by" {}
